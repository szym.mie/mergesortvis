const defpref = {
  chig: '#ff6600',
  cact: '#0066ff',
  cpsv: '#808080',
  fsiz: '25px',
  ffnt: 'monospace',
  bsiz: 40,
  bwid: 2,
  rsiz: 20,
  rwid: 1,
  rvof: 60,
  vbrc: 5
};
const dims = {x: 1000, y: 200};
const snaps = [];

// One snap : {o: length of original, c: [copy of current], g global{f: from, t: to}, a: active{f: from, t: to}, h: [highlights]}

function mergeSort(arr) {
  if (arr.length >= 2) {
    let l = arr.slice(0, arr.length / 2);
    let r = arr.slice(arr.length / 2);
//    pushSnap()
    mergeSort(l);
    pushSnap(snaps, snaps[snaps.length - 1].o, 0, arr.length / 2, arr, 0, arr.length / 2, [null]);
    mergeSort(r);
    pushSnap(snaps, snaps[snaps.length - 1].o, arr.length / 2, arr.length, arr, arr.length / 2, arr.length, [null]);

    let il = 0, ir = 0, i = 0;
    while (!(l[il] === undefined || r[ir] === undefined)) {
      console.log(`Sorting: ${arr}\nIL: ${il}\nIR: ${ir}`);
      l[il] > r[ir] ? (arr[i++] = r[ir++]) : (arr[i++] = l[il++]);
      pushSnap(snaps, snaps[snaps.length - 1].o, 0, arr.length, arr, i, i, [il, ir + arr.length / 2]);
    }

    while (!(l[il] === undefined)) {
      arr[i++] = l[il++];
      pushSnap(snaps, snaps[snaps.length - 1].o, 0, arr.length, arr, i, i, [i-1]);
    }
    while (!(r[ir] === undefined)) {
      arr[i++] = r[ir++];
      pushSnap(snaps, snaps[snaps.length - 1].o, 0, arr.length, arr, i, i, [i-1]);
    }
  }
}


function* getArray(str) {
  let arr = str.split(',');
  for (let i in arr) {
    let t = parseFloat(arr[i]);
    if (!isNaN(t)) yield t;
  }
}


function doOne() {
  try {
    let gen = getArray(arrInpId.value);
    let dt = dtInpId.value;
    console.log('Got array generator.');
    let mod = [...gen];
    while (snaps[0] !== undefined) snaps.shift();
    pushSnap(snaps, mod.length, 0, mod.length, mod, null, null, [null]);
    mergeSort(mod);
    pushSnap(snaps, mod.length, 0, mod.length, mod, 0, mod.length, [null]);
    drawSnaps(ctx, snaps, defpref, dims, dt);
    console.log(mod);
  } catch (err) {
    console.log('Input field has not loaded yet.');
  }
}


function pushSnap(snaps, aln, rs, re, arr, fr, to, hi) {
  snaps.push({o: aln, g: {f: rs, t: re}, c: arr.slice(), a: {f: fr, t: to}, h: hi});
}


function drawArray(ctx, snap, pref, dim) {
  ctx.font = `${pref.fsiz} ${pref.ffnt}`;
  let tot = snap.c.length * pref.bsiz;
  let stt = (dim.x - tot + pref.bwid) / 2;
  let tst = stt + (pref.bsiz - pref.bwid) / 2;
  let vup = (dim.y - pref.bsiz + pref.bwid) / 2;
  let siz = pref.bsiz - pref.bwid;
  let vct = dim.y / 2;
  console.log(`Total length: ${tot}\nHStart: ${stt}\nTStart: ${tst}\nVStart: ${vup}\nTrue size: ${siz}\nVCenter: ${vct}`);
  console.log(snap.c);
  ctx.lineWidth = pref.bwid;
  ctx.clearRect(0, 0, dim.x, dim.y);
  for (let b in snap.c) {
    if (isIn(snap.h, b)) {
      ctx.strokeStyle = pref.chig;
    } else if (b >= snap.a.f && b < snap.a.t) {
      ctx.strokeStyle = pref.cact;
    } else {
      ctx.strokeStyle = pref.cpsv;
    }
    console.log(ctx.strokeStyle, b);
    ctx.strokeRect(stt + b * pref.bsiz, vup, siz, siz);
    console.log(stt + b * pref.bsiz, vup, siz, siz);
    ctx.strokeText(snap.c[b], tst + b * pref.bsiz, vct);
    console.log(snap.c[b], tst + b * pref.bsiz, vct);
  }
}


function drawRange(ctx, snap, pref, dim) {
  let tot = snap.o * pref.rsiz;
  let stt = (dim.x - tot + pref.rwid) / 2;
  let vup = (dim.y - pref.rsiz + pref.rwid) / 2 - pref.rvof;
  let siz = pref.rsiz - pref.rwid;
  let vdw = dim.y / 2 - pref.rvof + pref.rsiz / 2;
  let sbr, ebr;
  ctx.strokeStyle = pref.cpsv;
  for (let b = 0; b < snap.o; b++) {
    ctx.strokeRect(stt + b * pref.rsiz, vup, siz, siz);
    console.log(stt + b * pref.rsiz, vup, siz, siz);
  }
  sbr = stt + Math.ceil(snap.g.f) * pref.rsiz;
  ebr = stt + Math.ceil(snap.g.t) * pref.rsiz - pref.rwid;
  console.log(sbr, ebr)
  ctx.strokeStyle = pref.chig;
  ctx.beginPath();
  ctx.moveTo(sbr, vdw);
  ctx.lineTo(sbr, vdw+pref.vbrc);
  ctx.lineTo(ebr, vdw+pref.vbrc);
  ctx.lineTo(ebr, vdw);
  ctx.stroke();
}


function isIn(s, a) {
  for (let i in a) {
    if (s == a[i]) return true;
  }
  return false;
}


function drawSnaps(ctx, snaps, pref, dim, td) {
  let j = 0;
  let i = setInterval(() => {
    drawArray(ctx, snaps[j++], pref, dim);
  }, td);
  let c = setTimeout(clearInterval, td * (snaps.length + .5), i);
}


var arrInpId, dtInpId, can, ctx;

window.addEventListener('load', () => {
  arrInpId = document.getElementById('arrInp');
  dtInpId = document.getElementById('dtInp');
  can = document.getElementById('canvas');
  ctx = can.getContext('2d');
  ctx.lineJoin = 'sharp';
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
});
